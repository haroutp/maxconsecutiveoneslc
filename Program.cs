﻿using System;

namespace MaxConsecutiveOnes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        public int FindMaxConsecutiveOnes(int[] nums) {
            int max = 0;
            int currentConsOnes = 0;
            
            for(int i = 0; i < nums.Length; i++){
                if(nums[i] == 1){
                    currentConsOnes++;
                    if(currentConsOnes > max){
                        max = currentConsOnes;
                    }
                }else {
                    currentConsOnes = 0;
                }
            }
        return max;
        }
    }
}
